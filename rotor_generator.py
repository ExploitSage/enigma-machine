#!/usr/bin/env python3
import argparse

# Rotor # 	ABCDEFGHIJKLMNOPQRSTUVWXYZ 	Date 	Model Name & Number
# I 		EKMFLGDQVZNTOWYHXUSPAIBRCJ 	1930 	Enigma I
# II 		AJDKSIRUXBLHWTMCQGZNPYFVOE 	1930 	Enigma I
# III 		BDFHJLCPRTXVZNYEIWGAKMUSQO 	1930 	Enigma I
# IV 		ESOVPZJAYQUIRHXLNFTGKDCMWB 	December 1938 	M3 Army
# V 		VZBRGITYUPSDNHLXAWMJQOFECK 	December 1938 	M3 Army
# VI 		JPGVOUMFYQBENHZRDKASXLICTW 	1939 	M3 & M4 Naval (FEB 1942)
# VII 		NZJHGRCXMYSWBOUFAIVLPEKQDT 	1939 	M3 & M4 Naval (FEB 1942)
# VIII 		FKQHTLXOCBJSPDZRAMEWNIUYGV 	1939 	M3 & M4 Naval (FEB 1942)

alphabet= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

def unordered_alphabet(x):
    unsorted_a = x.upper()
    sorted_a = ''.join(sorted(x))
    if sorted_a == alphabet:
        return unsorted_a
    raise ValueError('mapping must contain every letter A-Z once')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Enigma Rotor INI Generator.')
    parser.add_argument('mapping', type=unordered_alphabet, default='ABCDEFGHIJKLMNOPQRSTUVWXYZ', help='Rotor Mapping as String of Letters in Order to map to Alphabet')
    args = parser.parse_args();

    for i, letter in enumerate(args.mapping):
        print('%s=%s'%(alphabet[i], letter))
