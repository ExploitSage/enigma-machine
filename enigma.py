#!/usr/bin/env python3

import os
import sys
import configparser
import argparse

THIS_DIRECTORY = os.path.dirname(__file__)

ROTOR_FILE='rotors.ini'
ROTORS = configparser.ConfigParser()
ROTORS.read(os.path.join(THIS_DIRECTORY, ROTOR_FILE))

def parse_config(rotors):
    parser = argparse.ArgumentParser(description='Python Enigma Machine Implimentation.')
    parser.add_argument('-r', '--rotors', nargs='*', choices=rotors, default=rotors[0:2], help='Rotor names in order from left to right.')
    parser.add_argument('-p', '--positions', nargs='*', type=int, choices=range(1,27), help='Rotor starting positions in order from left to right.')
    args = parser.parse_args();

    if args.positions == None:
        args.positions = [1 for i in args.rotors]
    elif len(args.rotors) != len(args.positions):
        parser.error( 'Number of -p Positions need to match number of -r Rotors.')

    #TODO Plugboard Arguments

    return args

def enigma(rotors, positions, message):
    output = ''
    for letter in message:
        for rotor in rotors:
            pass
        for rotor in reversed(rotors):
            pass
        output += letter.upper()
    return output


if __name__ == '__main__':
    args = parse_config(ROTORS.sections())
    rotors = [dict(ROTORS.items(rotor)) for rotor in args.rotors]
    positions = args.positions
    message = sys.stdin.read().lower()
    print(enigma(rotors, positions, message))
